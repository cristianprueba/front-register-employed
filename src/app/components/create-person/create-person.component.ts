import { Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { PaisDTO } from 'src/app/models/pais.dto';
import { RegistroEmpleadosServices } from 'src/app/services/registro-empleados.service';

@Component({
  selector: 'app-create-person',
  templateUrl: './create-person.component.html',
  styleUrls: ['./create-person.component.scss'],
})
export class CreatePersonComponent implements OnInit {
  formPrincipal: UntypedFormGroup | undefined;
  paises: PaisDTO[] = [];

  constructor(
    public dialogRef: MatDialogRef<CreatePersonComponent>,
    private readonly registrarEmpleadosService: RegistroEmpleadosServices,
    private formBuilder: UntypedFormBuilder,
  ) {}

  ngOnInit(): void {
    this.buildForm();
    this.loadPaises();
  }

  loadPaises() {
    this.registrarEmpleadosService.getAllPaises().subscribe({
      next: (res) => {
        this.paises = res;
      },
    });
  }

  private buildForm() {
    this.formPrincipal = this.formBuilder.group({
      primerNombre: [''],
      otrosNombres: [''],
      primerApellido: [''],
      idPais: [null],
    });
  }
}

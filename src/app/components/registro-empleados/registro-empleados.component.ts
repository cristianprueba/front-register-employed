import { Component, OnInit } from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import { PersonaDTO } from 'src/app/models/persona.dto';
import { RegistroEmpleadosServices } from 'src/app/services/registro-empleados.service';
import { CreatePersonComponent } from '../create-person/create-person.component';
import { DialogAdvertenciaComponent } from '../dialog-advertencia/dialog-advertencia.component';

@Component({
  selector: 'app-registro-empleados',
  templateUrl: './registro-empleados.component.html',
  styleUrls: ['./registro-empleados.component.scss'],
})
export class RegistroEmpleadosComponent implements OnInit {
  personas: PersonaDTO[] = [];

  isLoadPersonas = false;

  constructor(
    private readonly registrarEmpleadosService: RegistroEmpleadosServices,
    public dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.loadPersonas();
  }

  openDialogCrearPersonaForm(
    enterAnimationDuration: string,
    exitAnimationDuration: string
  ): void {
    let dialog = this.dialog.open(CreatePersonComponent, {
      width: '600px',
      enterAnimationDuration,
      exitAnimationDuration,
    });
    dialog.afterClosed().subscribe((res) => {
      this.registrarEmpleadosService.createPerson(res.value).subscribe({
        next: (res) => {
          if (res != null) {
            this.loadPersonas();
          }
        },
      });
    });
  }

  openDialogAdvertencia(person: PersonaDTO): void {
    let dialog = this.dialog.open(DialogAdvertenciaComponent, {
      width: '600px',
      data: person
    });
    dialog.afterClosed().subscribe((res) => {
      if (res){
        this.deletePersona(res);
      }
    })
  }

  deletePersona(id: number) {
    this.registrarEmpleadosService.deletePerson(id).subscribe({
      next: (res) => {
        if (res) {
          this.loadPersonas();
        }
      },
    });
  }

  loadPersonas() {
    this.registrarEmpleadosService.getAllPersonas().subscribe({
      next: (res) => {
        this.personas = res;
        this.isLoadPersonas = true;
      },
    });
  }
}

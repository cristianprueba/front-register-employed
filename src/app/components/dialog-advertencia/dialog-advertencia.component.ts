import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { PersonaDTO } from 'src/app/models/persona.dto';

@Component({
  selector: 'app-dialog-advertencia',
  templateUrl: './dialog-advertencia.component.html',
  styleUrls: ['./dialog-advertencia.component.scss'],
})
export class DialogAdvertenciaComponent implements OnInit{

  persona: PersonaDTO | undefined;

  constructor(
    public dialogRef: MatDialogRef<DialogAdvertenciaComponent>,
    @Inject(MAT_DIALOG_DATA) private data: PersonaDTO
  ) {}

  ngOnInit(): void {
    this.persona = this.data;
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}

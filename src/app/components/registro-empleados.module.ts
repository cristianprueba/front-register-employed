import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RegistroEmpleadosRoutingModule } from './registro-empleados-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { MatDialogModule } from '@angular/material/dialog';
import { CreatePersonComponent } from './create-person/create-person.component';
import { RegistroEmpleadosComponent } from './registro-empleados/registro-empleados.component';
import { DialogAdvertenciaComponent } from './dialog-advertencia/dialog-advertencia.component';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
  declarations: [
    CreatePersonComponent,
    RegistroEmpleadosComponent,
    DialogAdvertenciaComponent,
  ],
  imports: [
    CommonModule,
    RegistroEmpleadosRoutingModule,
    FormsModule,
    HttpClientModule,
    MatDialogModule,
    ReactiveFormsModule,
    MatButtonModule,
  ],
})
export class RegistroEmpleadosModule {}

export interface PersonaDTO {
  id: 1;
  primerNombre: string;
  otrosNombres: string;
  primerApellido: string;
  idPais: number;
  correo: string;
}

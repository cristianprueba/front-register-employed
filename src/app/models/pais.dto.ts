export interface PaisDTO {
  id: number;
  codigo: string;
  nombre: string;
}

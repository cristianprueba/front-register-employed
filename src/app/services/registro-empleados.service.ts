import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { PaisDTO } from '../models/pais.dto';
import { environment } from 'src/environments/environment';
import { PersonaDTO } from '../models/persona.dto';

@Injectable({
  providedIn: 'root',
})
export class RegistroEmpleadosServices {
  constructor(private readonly http: HttpClient) {}

  getAllPaises(): Observable<PaisDTO[]> {
    return this.http.get<PaisDTO[]>(`${environment.registroClientes}/pais`);
  }

  getAllPersonas(): Observable<PersonaDTO[]> {
    return this.http.get<PersonaDTO[]>(
      `${environment.registroClientes}/persona`
    );
  }

  createPerson(persona: PersonaDTO) {
    return this.http.post<PersonaDTO>(
      `${environment.registroClientes}/persona`,
      persona
    );
  }

  deletePerson(id: number): Observable<any> {
    return this.http.delete<boolean>(
      `${environment.registroClientes}/persona/${id}`
    );
  }
}
